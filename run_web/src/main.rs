use web_server;
use std::fs;
use std::io::Read;

fn main() {    
    let http_server = web_server::new();
    http_server
        .get("/", Box::new(
            |_, mut response| {
                response.set_header("Content-Type", "text/html");
                let index_html = fs::read_to_string("./index.html")
                    .unwrap();
                response.set_body(&index_html);
                response
            }
        ))
        .get("/user", Box::new(
            |_, mut response| {
                response.set_header("Content-Type", "text/html");
                let index_html = fs::read_to_string("./index.html")
                    .unwrap();
                response.set_body(&index_html);
                response
            }
        ))
        .get("/pkg/package.json", Box::new(
            |_, mut response| {
                response.set_header("Content-Type", "application/json");
                let package_json = fs::read_to_string("./pkg/package.json")
                    .unwrap();
                response.set_body(&package_json);
                response
            }
        ))
        .get("/pkg/iceforever.js", Box::new(
            |_, mut response| {
                response.set_header("Content-Type", "application/javascript");
                let iceforever_js = fs::read_to_string("./pkg/iceforever.js")
                    .unwrap();
                response.set_body(&iceforever_js);
                response
            }
        ))
        .get("/pkg/iceforever_bg.js", Box::new(
            |_, mut response| {
                response.set_header("Content-Type", "application/javascript");
                let iceforever_bg_js
                    = fs::read_to_string("./pkg/iceforever_bg.js")
                    .unwrap();
                response.set_body(&iceforever_bg_js);
                response
            }
        ))
        .get("/pkg/iceforever_bg.wasm", Box::new(
            |_, mut response| {
                response.set_header("Content-Type", "application/wasm");
                let mut wasm_file
                    = fs::File::open("./pkg/iceforever_bg.wasm")
                    .unwrap();
                let mut wasm_buf: Vec<u8> = Vec::new();
                let _ = wasm_file.read_to_end(&mut wasm_buf).unwrap();
                response.set_raw_body(wasm_buf.clone());
                response
            }
        ))
        .launch(8080);
}
