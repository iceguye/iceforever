mod painter;
mod wgpu_state;

use crate::{
    wgpu_state::WgpuState,
};

use winit::{
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::{Window},
};
use winit::dpi::{PhysicalSize};
use log::warn;
use wasm_bindgen::prelude::*;
use egui;
use egui_winit;
use egui_wgpu;
use std::sync::Arc;
use wgpu;

#[cfg_attr(target_arch="wasm32", wasm_bindgen(start))]
pub async fn web_run() {
    use winit::platform::web::WindowExtWebSys;
    
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
    console_log::init_with_level(log::Level::Warn)
        .expect("Couldn't initialize logger");
    let event_loop = EventLoop::new().unwrap();
    let window = Window::new(&event_loop).unwrap();
    let window = Arc::new(window);
    event_loop.set_control_flow(ControlFlow::Poll);
    // let _ = window.request_inner_size(PhysicalSize::new(1280, 720));
    let mut web_window = web_sys::window();
    let url = web_window.clone().unwrap().document().unwrap().url().unwrap();
    
    web_window.as_mut().and_then(|win| win.document())
        .and_then(|doc| {
            let dst = doc.get_element_by_id("iceforever-wasm")?;
            let canvas = web_sys::Element::from(window.canvas().unwrap());
            dst.append_child(&canvas).ok()?;
            Some(())
        })
        .expect("Couldn't append canvas to document body.");

    let mut wgpu_state = WgpuState::new(window.clone()).await;

    let mut ctx = egui::Context::default();
    let viewport_id = ctx.clone().viewport_id().clone();
    let mut ews = egui_winit::State::new(
        ctx.clone(),
        viewport_id,
        &window,
        None,
        None
    );

    let wgpu_state = WgpuState::new(window.clone()).await;
    let mut egui_painter = painter::Painter::new(&wgpu_state);

    // Main loop starts from here:
    let _ = event_loop.run(move |event, _elwt| {
        let inner_height = web_window.as_ref().unwrap()
            .inner_height().unwrap().as_f64().unwrap();
        let inner_width = web_window.as_ref().unwrap()
            .inner_width().unwrap().as_f64().unwrap();
        let _ = window.request_inner_size(
            PhysicalSize::new(inner_width, inner_height)
        );
        
        match event {
            _ => {},
        }
        let raw_input: egui::RawInput = egui::RawInput::default();
        let full_output = ctx.run(raw_input, |ctx| {
            egui::CentralPanel::default().show(&ctx, |ui| {
                ui.label("Hello world!");
                if ui.button("Click me").clicked() {
                    // take some action here
                }
            });
        });
        let clipped_primitives = ctx.tessellate(
            full_output.shapes,
            full_output.pixels_per_point
        );
        egui_painter.update_textures(&full_output.textures_delta,
                                     &wgpu_state);
        egui_painter.render(&wgpu_state,
                            &clipped_primitives,
                            &window.clone());
    });
}
