use crate::{
    wgpu_state::WgpuState,
};

use egui::TexturesDelta;
use egui::ClippedPrimitive;
use egui_wgpu;
use std::sync::{Arc, Mutex};
use winit::{
    window::{Window},
};

pub struct Painter {
    renderer: egui_wgpu::Renderer,
}

impl Painter {
    pub fn new(wgpu_state: &WgpuState<'_>) -> Self {
        let renderer = egui_wgpu::Renderer::new(
            &wgpu_state.device.clone(),
            wgpu::TextureFormat::Rgba8Unorm,
            None,
            1,
        );

        Self {
            renderer,
        }
    }

    pub fn update_textures(&mut self,
                           textures_delta: &TexturesDelta,
                           wgpu_state: &WgpuState<'_>,) {
        for (texture_id, image_delta) in textures_delta.set.iter() {
            self.renderer.update_texture(
                &wgpu_state.device.clone(),
                &wgpu_state.queue.clone(),
                *texture_id,
                &image_delta,
            );
        }
    }

    pub fn render(&mut self,
                  wgpu_state: &WgpuState<'_>,
                  clipped_primitives: &[ClippedPrimitive],
                  window: &Window) {
        let mut command_encoder = wgpu_state.device
            .create_command_encoder(
                &wgpu::CommandEncoderDescriptor {
                    label: Some("Render Encoder"),
                }
            );
        let wsize = window.inner_size();
        let screen_descriptor = egui_wgpu::ScreenDescriptor {
            size_in_pixels: [wsize.width, wsize.height],
            pixels_per_point: 1.0
        };
        let _ = self.renderer.update_buffers(
            &wgpu_state.device.clone(),
            &wgpu_state.queue.clone(),
            &mut command_encoder,
            clipped_primitives,
            &screen_descriptor,
        );
    }
}
