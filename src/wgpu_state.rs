use std::sync::Arc;
use winit::{
    window::{Window},
};

pub struct WgpuState<'a> {
    pub surface: Arc<wgpu::Surface<'a>>,
    pub device: Arc<wgpu::Device>,
    pub queue: Arc<wgpu::Queue>,
}

impl WgpuState<'_> {
    pub async fn new (window: Arc<Window>) -> Self {
        let instance = wgpu::Instance::new(wgpu::InstanceDescriptor {
            backends: wgpu::Backends::all(),
            ..Default::default()
        });
        let surface = instance.create_surface(window.clone()).unwrap();
        let surface = Arc::new(surface);
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(),
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .await
            .unwrap();
        let (device, queue) = adapter
            .request_device (
                &wgpu::DeviceDescriptor{
                    required_features: wgpu::Features::empty(),
                    required_limits: if cfg!(target_arch = "wasm32") {
                        wgpu::Limits::downlevel_webgl2_defaults()
                    } else {
                        wgpu::Limits::default()
                    },
                    label: None,
                },
                None, 
            ).await.unwrap();
        let device = Arc::new(device);
        let queue = Arc::new(queue);
        Self {
            surface: surface.clone(),
            device: device.clone(),
            queue: queue.clone(),
        }
    }
}
